
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class TicTacToe {

    static ArrayList<Integer> playerPositions = new ArrayList<Integer>();
    static ArrayList<Integer> cpuPositions = new ArrayList<Integer>();
    public static void main(String[] args) {
        //create 2d array of chars
        char [][] gameBoard = {{' ', '|', ' ', '|', ' '},
        {'-', '+', '-', '+', '-'},
        {' ', '|', ' ', '|', ' '},
        {'-', '+', '-', '+', '-'},
        {' ', '|', ' ', '|', ' '}};

        printGameBoard(gameBoard);

        //Create scanner to get input from the user on where they
        //would like to place their X or O.
       
        while(true) {
            Scanner scanner = new Scanner(System.in);
            
            System.out.println("Enter your placement(1-9):");
            int playerPos = scanner.nextInt();
            
            piecePlacement(gameBoard, playerPos, "player");

            Random rand = new Random();
            int cpuPos = rand.nextInt(9)+1;
            piecePlacement(gameBoard, cpuPos, "cpu");

            printGameBoard(gameBoard);

            checkWinner();
            
        }
        // scanner.close();
    }

    public static void printGameBoard(char[][] gameBoard) {
        for (char[] row : gameBoard) {
            for (char c : row) {
                System.out.print(c);
            }
            System.out.println();
        }
    }

    public static void piecePlacement(char[][] gameBoard, int pos, String user) {
                //Create a switch case for each position on the board, 1-9.
            char symbol = ' ';
            //Distinguish which player is which so we add add X's or O's.
            if(user.equals("player")) {
                symbol = 'X';

            }else if(user.equals("cpu")) {
                symbol = 'O';
            }
            
                switch(pos) {
                    case 1:
                    gameBoard[0][0] = symbol;
                    break;
                    case 2:
                    gameBoard[0][2] = symbol;
                    break;
                    case 3:
                    gameBoard[0][4] = symbol;
                    break;
                    case 4:
                    gameBoard[2][0] = symbol;
                    break;
                    case 5:
                    gameBoard[2][2] = symbol;
                    break;
                    case 6:
                    gameBoard[2][4] = symbol;
                    break;
                    case 7:
                    gameBoard[4][0] = symbol;
                    break;
                    case 8:
                    gameBoard[4][2] = symbol;
                    break;
                    case 9:
                    gameBoard[4][4] = symbol;
                    break;
                    default:
                    break;
                    }
    }
    public static String checkWinner() {
        List topRow = Arrays.asList(1,2,3);
        List midRow = Arrays.asList(4,5,6);
        List botRow = Arrays.asList(7,8,9);
        List leftCol = Arrays.asList(1,4,7);
        List midCol = Arrays.asList(2,5,8);
        List rightCol = Arrays.asList(3,6,9);
        List cross1 = Arrays.asList(1,5,9);
        List cross2 = Arrays.asList(7,2,3);

        List<List> winWin = new ArrayList<List>();
        winWin.add(topRow);
        winWin.add(midRow);
        winWin.add(botRow);
        winWin.add(leftCol);
        winWin.add(midCol);
        winWin.add(rightCol);
        winWin.add(cross1);
        winWin.add(cross2);

        for (List l : winWin) {
            if(playerPositions.containsAll(l)){
                return "Yay! You Won!";
            }else if(cpuPositions.containsAll(l)) {
                return "Sorry you lose, cpu won!";
            }else if(playerPositions.size() + cpuPositions.size() ==9) {
                return "TIE";
        
            }
        return "";
        }
    }
}
